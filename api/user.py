# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify, request, url_for, redirect, abort
from flask import make_response, g

from helpers import authorization_required, authentication_required

from todoapp.models import User
from todoapp import db


bp = Blueprint('user', __name__, url_prefix='/api')

@bp.route('/users', methods=['GET'])
@authorization_required
def get_users():
    users = User.query.all()
    users = [user.to_dict() for user in users]
    return jsonify({'users': users})


@bp.route('/user', methods=['POST'])
def create_user():
    json = request.get_json()
    if all([json.get(key) for key in ['username', 'email', 'password']]):
        user = User.query.filter_by(username=json.get('username')).first()
        if not user:
            user = User(
                    username=json.get('username'),
                    email=json.get('email'),
                    )
            user.set_password(json.get('password'))
            db.session.add(user)
            db.session.commit()
            return make_response(jsonify(user.to_dict()), 201)
        return make_response(jsonify({'error':'user already exists'}), 406)
    return make_response(jsonify({'error':'missing arguments'}), 406)


@bp.route('user/delete', methods=['GET'])
@authorization_required
def delete():
    user = g.current_user
    if user:
        db.session.delete(user)
        db.session.commit()
        return jsonify({'result':'successful'})
    return make_response(jsonify({'error':'not found'}), 404)


@bp.route('/user', methods=['PUT'])
@authorization_required
def update():
    user = g.current_user
    if user:
        json_data = request.get_json()
        # get only valid fields that contain data
        for field in [field for field in ['username', 'email', 'password']
        # if it contains data update that field on the database
        if json_data.get(field)]:
            if field == 'password':
                user.set_password(json_data.get(field))
            user.__setattr__(field, json_data.get(field))
        db.session.add(user)
        db.session.commit()
        return jsonify(user.to_dict())
    return make_response(jsonify({'error':'not found'}), 404)


@bp.route('/user', methods=['GET'])
@authorization_required
def get_user():
    user = g.current_user
    if user:
        return jsonify(user.to_dict())
    return make_response(jsonify({'error':'not found'}), 404)


@bp.route('/user/token', methods=['POST'])
@authentication_required
def get_user_token():
    data = {'Token': g.current_user.token}
    return jsonify(data)


@bp.route('/user/token/reset', methods=['POST'])
@authentication_required
def reset_user_token():
    user = g.current_user
    user.create_user_token()
    db.session.add(user)
    db.session.commit()
    return jsonify({'Token': user.token})

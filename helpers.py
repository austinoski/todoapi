from functools import wraps

from flask import jsonify, make_response, request, g

from todoapp.models import User


# returns 401 error if user is not authorized
def authorization_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        json = request.get_json()
        if json and json.get('Token'):
            user = User.query.filter_by(token=json['Token']).first()
            if user:
                g.current_user = user
                return func(*args, **kwargs)
        return make_response(jsonify({'error':'you are not authorized'}), 401)
    return wrapper


# returns a 404 error if user authentication failed
def authentication_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        json = request.get_json()
        username = json.get('username')
        password = json.get('password')
        user = User.query.filter_by(username=username).first()
        if user and user.check_password(password):
            g.current_user = user
            return func(*args, **kwargs)
        return make_response(jsonify({'error':'Authentication Failed'}), 404)
    return wrapper

# -*- coding: utf-8 -*
import os, base64

from werkzeug.security import generate_password_hash, check_password_hash

from todoapp import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(16), unique=True, index=True)
    email = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(256))
    token = db.Column(db.String(64), unique=True, index=True)
    
    def __init__(self, username, email, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.username = username
        self.email = email
        self.create_user_token()
    
    def __repr__(self):
        return '<{}>'.format(self.username)
    
    def to_dict(self):
        user = {}
        for field in ['id', 'username', 'email']:
            user[field] = self.__getattribute__(field)
        return user
    
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def create_user_token(self):
        token = base64.b64encode(os.urandom(64)).decode('utf-8')
        self.token = token
        
    